---
home: true
heroImage: /logo/logo.png
heroText: EightRoes
tagline: 🚀基于SpringBoot的插件式快速开发框架
actionText: 开始使用 →
actionLink: /pages/0101/
bannerBg: none # auto => 网格纹背景(有bodyBgImg时无背景)，默认 | none => 无 | '大图地址' | background: 自定义背景样式       提示：如发现文本颜色不适应你的背景时可以到palette.styl修改$bannerTextColor变量

features:
  - title: 技术栈
    details: 使用 SpringBoot、Mabatis-plus、Vue 等前后端前沿技术开发。
  - title: 插件化
    details: 是基于插件的可插拔的松耦合体系，提升开发，测试效率。
  - title: 高效率
    details: 已经集成了基础平台插件，包含了用户，机构，角色，权限，定时任务等功能。
  - title: 分离式
    details: 前后端完全分离，前端基于 Vue3，后端基于 Spring boot。
  - title: 响应式
    details: 支持电脑、平板、手机等所有主流设备访问。
  - title: 易用性
    details: 几乎可用于所有Web项目的开发，如 OA、Cms，网址后台管理等。

# 文章列表显示方式: detailed 默认，显示详细版文章列表（包括作者、分类、标签、摘要、分页等）| simple => 显示简约版文章列表（仅标题和日期）| none 不显示文章列表
postList: none
---

<p align="center">
  <a class="become-sponsor" href="/pages/1b12ed/">支持这个项目</a>
</p>

<style>
.become-sponsor{
  padding: 8px 20px;
  display: inline-block;
  color: #11a8cd;
  border-radius: 30px;
  box-sizing: border-box;
  border: 1px solid #11a8cd;
}
</style>

<br/>
<p align="center">
  <a href='https://gitee.com/quella01/EightRoes/stargazers'><img src='https://gitee.com/quella01/EightRoes/badge/star.svg?theme=dark' alt='star'></img></a>
 <a href='https://gitee.com/quella01/EightRoes/members'><img src='https://gitee.com/quella01/EightRoes/badge/fork.svg?theme=dark' alt='fork'></img></a>
</p>

<br/>

## ⚡ 反馈与交流

在使用过程中有任何问题和想法，请给我提 [Issue](https://gitee.com/quella01/EightRoes/issues)。
你也可以在 Issue 查看别人提的问题和给出解决方案。

<!-- 或者加入我们的交流群：

<table>
  <tbody>
    <tr>
      <td align="center" valign="middle">
        <img src="https://cdn.jsdelivr.net/gh/xugaoyi/image_store2@master/img/mmqrcode1646409021986.5m4pb5hehz80.png" class="no-zoom" style="width:120px;margin: 10px;">
        <p>vdoing微信群(添加我的微信进群)</p>
      </td>
      <td align="center" valign="middle">
        <img src="https://cdn.jsdelivr.net/gh/xugaoyi/image_store@master/qq.3ugglfuuwz00.webp" alt="群号: 694387113" class="no-zoom" style="width:120px;margin: 10px;">
        <p>vdoing QQ群: 694387113</p>
      </td>
    </tr>
  </tbody>
</table> -->

<!-- Happy new year -->
<!-- <br/><br/>
<div class="container-happy">
  <div>
    <span>Happy</span>
    <span>Wish</span>
  </div>
    <div>
    <span>New</span>
    <span>You</span>
  </div>
  <footer>
      <div>
    <span>Year</span>
    <span>Luck</span>
  </div>
  <div>
    <span>2022</span>
    <span>Tomorrow</span>
  </div>
  </footer>
</div>

<style>
.container-happy {
  font-size: 18px;
  font-family: Times New Roman;
  perspective: 35rem;
  width: 100%;
  margin: 0 auto;
  color: tomato;
  opacity: 0.8;
}

.container-happy footer {
  perspective: 35rem;
  transform: translateY(-1.4rem);
}

.container-happy div {
  font-size: 5rem;
  height: 6rem;
  overflow: hidden;
  text-transform: uppercase;

}

.container-happy div>span {
  display: block;
  height: 6rem;
  padding: 0 1rem;
  font-weight: bold;
  letter-spacing: .2rem;
  text-align: center;
  transition: .3s;
}

.container-happy:hover div>span {
  transform: translateY(-100%);
}

.container-happy div:nth-child(odd) {
  background-color: #EBFCFF;
  transform: rotateX(30deg);
}

.container-happy div:nth-child(even) {
  background-color: #E6F4F1;
  transform: translateY(-.6rem) rotateX(-30deg);
}
</style> -->

<!-- AD -->
<!-- <div class="wwads-cn wwads-horizontal pageB" data-id="136" style="width:100%;max-height:80px;min-height:auto;"></div>
<style>
  .pageB img{width:80px!important;}
  .pageT .wwads-content{display:flex;align-items: center;}
  .pageT .wwads-poweredby{display:none!important;}
  .pageT .wwads-hide{display:none!important;}
</style> -->
